

## Sample Project

**Step 1:** clone the repository 

``` bash
$ git clone https://abhilash-kp@bitbucket.org/abhilash-kp/task.git
```
**Step 2:** Install php dependencies
``` bash
$ composer install
``` 
**Step 3:** Install node dependencies
``` bash
$ npm install

$ npm run dev 
```
**Step 4:** Create virtual host for the application  
To use social logins, we have to create the virtual hosts.  
**Step 5:** Edit the .env file to connect to database    
- ``DB_DATABASE=your Database Name``   
- ``DB_USERNAME=your database username``  
- ``DB_PASSWORD=your database password``   

**Step 6:** migrate the database tables  
``` bash
$ php artisan migrate 
```
**Step 7:** To login using facebook or linkedin, Edit your .env file  

- ``FACEBOOK_CLIENT_ID=your facebook client id here``   
- ``FACEBOOK_CLIENT_SECRET=your facebook client secret here ``  
- ``FACEBOOK_CALLBACK_URL=your facebook call back url``   

And similarly for Linkedin.  

**Step 8: **create a mailtrap.io Account so that all mails sent will be seen in one place.Edit the .env file    
- `` MAIL_USERNAME= user name``  
- ``MAIL_PASSWORD= password``  

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MSValue extends Model
{
    protected $table="MSValues";
    protected $fillable = ['value'];
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SocialProvider extends Model
{
    protected $table="socialproviders";
    //fields that are mass assignable
    protected $fillable = ['user_id','name','provider','social_id','email'];

    //This social provider belongs to specific user
    public function user(){

        return $this->belongsTo(User::class);

    }
}

<?php

namespace App\Http\Controllers\Auth;

use App\Events\Registered;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use App\SocialProvider;
use Laravel\Socialite\Facades\Socialite;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Redirects the user to the social providers page for authentication.
     * @param $socialProvider
     * @return mixed
     */
    public function redirectToProvider($socialProvider){
        return Socialite::driver($socialProvider)->redirect();
    }

    /**
     * Get user details from social provider
     * @param Request $request
     * @param $socialProvider
     * @return \Illuminate\Http\RedirectResponse|void
     */
    public function handleCallback(Request $request,$socialProvider){

        // If user cancels the authentication in provider(facebook) authentication page then
        // user will be redirected to home page.
        if (!$request->has('code') || $request->has('denied')) {
            return redirect('/login');
        }
        try{
            //get user details from social providers
            $user = Socialite::driver($socialProvider)->user();
        }catch (\Exception $exception){
            // if something happens while getting user details then redirect to login page
            return redirect('/login');
        }
        //if email not present in the user details then redirect the user back to login page.
        if($user->email){

            $mainUser = User::where('email',$user->email)->first();
            if($mainUser){ //if email present in users table then check in socialproviders table
                $socialUser = Socialprovider::where(['social_id' => $user->id, 'provider' => $socialProvider])->first();
                if($socialUser){
                    Auth::login($mainUser);
                    return redirect()->route('home')->with('status','logged in');
                }else{
                    $mainUser->socialproviders()->create([
                        'name' => $user->name,
                        'provider' => $socialProvider,
                        'social_id' => $user->id,
                        'email' => $user->email
                    ]);
                    Auth::login($mainUser);
                    return redirect()->route('home')->with('status','logged in');
                }

            } else {
                $mainUser = User::create([

                    'name' => $user->name,
                    'email' => $user->email,

                ]);
                $mainUser->socialproviders()->create([
                    'name' => $user->name,
                    'provider' => $socialProvider,
                    'social_id' => $user->id,
                    'email' => $user->email
                ]);
                Auth::login($mainUser);
                event(new Registered($mainUser));
                return redirect()->route('home')->with('status','logged in');

            }

        }else{
            return redirect('/login')->with('error','Email id not Found, Try another Social login or Register with us.');
        }


    }
}

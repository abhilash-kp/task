<?php

namespace App\Http\Controllers;

use App\MSValue;
use Illuminate\Http\Request;

class MSValueController extends Controller
{
    public function saveValue(Request $request){

       $msvalue = $request->validate([
            'value' => 'required|alpha_num|max:50'
        ]);

       MSValue::create($msvalue);
       return redirect()->route('home')->with('status','Value inserted to database Successdully');
    }
}

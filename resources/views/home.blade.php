@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">This is My Home Page</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif


                        <form class="form-horizontal" method="POST" action="{{ URL::to('/msvalue') }}">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('value') ? ' has-error' : '' }}">
                                <label for="value" class="col-md-4 control-label">Value</label>

                                <div class="col-md-6">
                                    <input id="value" type="text" class="form-control" name="value" value="{{ old('value') }}" required autofocus>

                                    @if ($errors->has('value'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('value') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Save
                                    </button>
                                </div>
                            </div>
                        </form>



                </div>
            </div>
        </div>
    </div>
</div>
@endsection
